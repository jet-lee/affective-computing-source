# Notes from lesson on 2022/05/04

## Materials for Affective Computing Course

```
https://www.mobile.ifi.lmu.de/lehrveranstaltungen/affective-computing-sose22/
```

## HOW-TO

### Presentation

- look in folder **slides**

### Test OpenCV

- look in foler **opencv** (more infos in **opencv/README.md**)

### Test notebooks

- look in folder **notebooks** and test files in local environment or on google colab (more infos in **notebooks/README.md**)

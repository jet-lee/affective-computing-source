import dlib
import cv2
import numpy as np


face_cascade = cv2.CascadeClassifier('../assets/haarcascade_frontalface_default.xml')

landmark_model = dlib.shape_predictor('../assets/shape_predictor_68_face_landmarks.dat')


cap = cv2.VideoCapture(0)

while (cap.isOpened()):

    # Capture frame-by-frame
    ret, frame = cap.read()


    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 2, minSize=(200, 200))


    for (x1, y1, w, h) in faces:

        x2 = x1 + w
        y2 = y1 + h 

        pad = 0
        x1 = int(x1 - pad)
        x2 = int(x2 + pad)
        y1 = int(y1 - pad)
        y2 = int(y2 + pad)

        face = frame[y1:y2, x1:x2]


        cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
        shape = landmark_model(face, dlib.rectangle(0, 0, face.shape[1], face.shape[0]))

        for i in range(0, 68):
            (cx, cy) = (int(shape.part(i).x), int(shape.part(i).y))
            cv2.circle(frame, (x1+ cx, y1 + cy), 1, (0, 255, 0), 6)


    cv2.imshow("frame", frame)	

    k = cv2.waitKey(1)
    if k == 27:
        break


# When everything done, release the capture
cv2.destroyAllWindows()
cap.release()

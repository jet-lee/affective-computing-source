import cv2

cap = cv2.VideoCapture(0)

# Infinite loop until we hit the escape key on keyboard
while cap.isOpened():

    ret, frame = cap.read()

    # display images
    cv2.imshow('original', frame)

    key = cv2.waitKey(1)
    if key == 27:
        break

cv2.destroyAllWindows()
cap.release()



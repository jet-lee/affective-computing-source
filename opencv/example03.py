import cv2

cap = cv2.VideoCapture(0)


# this function is needed for the createTrackbar step downstream
def on_change(x):
    pass

# create trackbar for canny edge detection threshold changes
cv2.namedWindow('canny')

# add lower and upper threshold slidebars to "canny"
cv2.createTrackbar('lower', 'canny', 0, 255, on_change)
cv2.createTrackbar('upper', 'canny', 0, 255, on_change)

# Infinite loop until we hit the escape key on keyboard
while cap.isOpened():

    ret, frame = cap.read()

    # get current positions of four trackbars
    lower = cv2.getTrackbarPos('lower', 'canny')
    upper = cv2.getTrackbarPos('upper', 'canny')

    edges = cv2.Canny(frame, lower, upper)

    # display images
    cv2.imshow('original', frame)
    cv2.imshow('canny', edges)

    k = cv2.waitKey(1)
    if k == 27:
        break

cv2.destroyAllWindows()
cap.release()

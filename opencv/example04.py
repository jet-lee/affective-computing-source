import cv2

cap = cv2.VideoCapture(0)

face_cascade = cv2.CascadeClassifier('../assets/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('../assets/haarcascade_eye.xml')

while (cap.isOpened()):

    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    # lines, columns, _ = frame.shape
    # frame = cv2.resize(frame, (int(columns / 2), int(lines / 2)))

    gray = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 2, minSize=(200, 200))

    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

        roi_gray = gray[y:y + h, x:x + w]
        roi_color = frame[y:y + h, x:x + w]
        eyes = eye_cascade.detectMultiScale(roi_gray, 1.1, 2, minSize=(1,1))
        for (ex, ey, ew, eh) in eyes:
            cv2.circle(roi_color, (int(ex+ew/2), int(ey+ey/2)), 6,  (0, 255, 0), -1)

    cv2.imshow("frame", frame)

    k = cv2.waitKey(1)
    if k == 27:
        break

# When everything done, release the capture
cv2.destroyAllWindows()
cap.release()

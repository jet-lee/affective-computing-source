## About OpenCV and dlib

```
https://opencv.org
https://pypi.org/project/opencv-python/

http://dlib.net/
https://pypi.org/project/dlib/
```

## Install OpenCV and dlib for Python

### Requirements

- install python3 and pip for python3

### Install Python environment

#### Linux / MacOS

```
python3 -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip setuptools wheel
```

#### Windows

- see following link [https://docs.python.org/3/library/venv.html](https://docs.python.org/3/library/venv.html)


### Install necessary packages

```
pip install opencv-python
pip install dlib
```



import cv2

filePath = "../assets/test-img.jpg"


img1 = cv2.imread(filePath)

cv2.imshow("image1", img1)

img2 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
cv2.imshow("image2", img2)

cv2.waitKey(0)
cv2.destroyAllWindows()

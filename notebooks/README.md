## HOW TO USING JUPYTER NOTEBOOK

### Requirements

- install python3 and pip for python3

### Install Python environment

```
python3 -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip setuptools wheel
```

### Install necessary packages

```
pip install -r requirement.txt

```
